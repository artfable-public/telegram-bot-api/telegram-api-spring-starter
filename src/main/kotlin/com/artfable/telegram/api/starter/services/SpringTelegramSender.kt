package com.artfable.telegram.api.starter.services

import com.artfable.telegram.api.TelegramBotMethod
import com.artfable.telegram.api.TelegramResponse
import com.artfable.telegram.api.TelegramServerException
import com.artfable.telegram.api.request.TelegramRequest
import com.artfable.telegram.api.service.AbstractTelegramSender
import com.artfable.telegram.api.starter.utils.getUri
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.ParameterizedTypeReference
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.stereotype.Service
import org.springframework.util.CollectionUtils
import org.springframework.web.client.RestTemplate

/**
 * @author aveselov
 * @since 10/02/2021
 */
@Service
class SpringTelegramSender(
    @Value("\${telegram.bot.token}")
    private val token: String,

    private val restTemplate: RestTemplate
) : AbstractTelegramSender() {
    override fun <T> send(telegramRequest: TelegramRequest<T>): TelegramResponse<T>? {
        try {
            return restTemplate
                .exchange(
                    getUri(URL, getUrlParams(telegramRequest.method)),
                    HttpMethod.POST,
                    convertEntity(telegramRequest.asEntity()),
                    ParameterizedTypeReference.forType<TelegramResponse<T>>(telegramRequest.responseType)
                )
                .body
        } catch (e: Exception) {
            throw TelegramServerException("Can't execute request", e);
        }
    }

    private fun getUrlParams(method: TelegramBotMethod): Map<String, String?> {
        return mapOf(
            "token" to token,
            "method" to method.value
        )
    }

    private fun convertEntity(entity: TelegramRequest.TelegramRequestEntity): HttpEntity<*> {
        return HttpEntity(entity.body, entity.headers?.let { HttpHeaders(CollectionUtils.toMultiValueMap(it)) })
    }
}