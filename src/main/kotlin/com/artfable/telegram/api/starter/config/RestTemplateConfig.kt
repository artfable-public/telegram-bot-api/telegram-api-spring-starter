package com.artfable.telegram.api.starter.config

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.http.HttpStatus
import org.springframework.http.HttpStatusCode
import org.springframework.web.client.DefaultResponseErrorHandler
import org.springframework.web.client.ResponseErrorHandler
import org.springframework.web.client.RestTemplate

/**
 * Auto configuration that provide [RestTemplate] for a bot.
 * Will be enable by EnableAutoConfiguration with Spring Boot
 *
 * @author aveselov
 * @since 06/08/2020
 */
@Configuration
class RestTemplateConfig {
    @Bean
    fun telegramBotRestTemplate(): RestTemplate {
        val restTemplate = RestTemplate()
        restTemplate.errorHandler = telegramResponseErrorHandler()
        return restTemplate
    }

    fun telegramResponseErrorHandler(): ResponseErrorHandler {
        return object : DefaultResponseErrorHandler() {
            override fun hasError(statusCode: HttpStatusCode): Boolean {
                return !statusCode.is2xxSuccessful && !statusCode.is4xxClientError && super.hasError(statusCode)
            }
        }
    }
}
