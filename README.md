# Telegram Bot Api Spring Boot Starter
Spring Boot starter for [Telegram Api](https://gitlab.com/artfable-public/telegram-bot-api/telegram-api-java/)

 - Java 17
 - Spring boot 3.1.0

## Dependency

```kotlin
repository {
    maven(url = "https://gitlab.com/api/v4/groups/68820060/-/packages/maven")
}

dependecies {
    implementation("com.artfable.telegram:telegram-api-spring-starter:0.2.0")
}
```

## Usage

For start create instance of LongPollingTelegramBotSpringWrapper or WebhookTelegramBotSpringStarter.

See [Telegram Api README](https://gitlab.com/artfable-public/telegram-bot-api/telegram-api-java/-/blob/master/README.md)

### TelegramSender

Property **telegram.bot.token** should be provided for Spring context to configure the sender.

### Commands configuration

To set up bots commands, provide a json file with commands description to property _telegram.bot.commands_.

File example:

```json
[
  {
    "command": "start",
    "description": "Start bot process"
  }
]
``` 